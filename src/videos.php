<?php
header('Content-Type: text/html; charset=utf-8');
include_once "header.php";

include_once "functions.php";


?>





    <div class="row clearfix">
        <div class="col-md-3 column">
            <h3><span id="cabecalho_menu_lateral">NEPOPS</span></h3>
            <hr>
        </div>
        <div class="col-md-9 column">
            <h3><span id="titulo_noticia" >Vídeos</span></h3>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-3 column">
            <div class="list-group">


                <p class="list-group-item-text">
                <ul class="list-group" id="estilo_menu_lateral">
                    <li class="list-group-item"><a href="historia.php?id=11">História</a></li>
                    <li class="list-group-item"><a href="projetos.php">Projetos</a></li>
                    <li class="list-group-item" ><a href="noticias.php" >Notícias</a></li>
                    <li class="list-group-item" ><a href="biblioteca.php" >Biblioteca</a></li>
                    <li class="list-group-item"><a href="photos_nepops.php">Fotos</a></li>
                    <li class="list-group-item"  style="background-color:#5b62ab;"><a href="videos.php"  style="color:white;">Vídeos</a></li>
                    <li class="list-group-item"><a href="#">Contatos</a></li>
                </ul>
                </p>


            </div>
        </div>
        <div class="col-md-9 column" id="dados">
            <div id="conteudo">
                <?php
                include_once "functions.php";

                $videos = getAllVideos();
                if($videos){
                    $retorno = "";
                    foreach ($videos as $key => $value) {
                        //$valor = split("=",$value->link);
                        $valor = explode("=",$value->url);
                        $re = "/[be\\?\\&](\/|v=)([^\\?\\&]+)/";
                        preg_match($re, $value->url, $matches);


                        $retorno .= "<div class='row clearfix' style='margin-bottom: 1.2em;''><div class='col-md-6 column'><iframe width='420' height='251' src='https://www.youtube.com/embed/".$matches[2]."?autoplay=0&controls=2' frameborder='0' allowfullscreen></iframe>";
                        $retorno .="</div><div class='col-md-6 column'><h3  class='titleProject' style='margin-top:0px;'><a href=''>".$value->titulo."</a></h3></div></div>";
                    }
                    echo $retorno;
                }else{
                    echo "Não possui videos";
                }


                ?>
            </div>

            <script type="text/javascript">
                <?php echo "var qnt = ".(string)ceil(getCountVideos()->count/4).";";?>
                function nextPage(){

                    var page = parseInt($('#page').text());
                    var count = qnt;

                    if(page < count)
                        setPage(page);
                }

                function prevPage(){

                    var page = parseInt($('#page').text());
                    if(page > 1)
                        setPage(page-2);


                }
                function setPage(x){
                    var p =  x + 1;
                    funcao_ajax(p);
                    $( "#page" ).html(p);
                    document.getElementById("conteudo").innerHTML="";
                }

                function funcao_ajax(pagina)
                {
                    var xmlhttp;
                    if (window.XMLHttpRequest)
                    {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp=new XMLHttpRequest();
                    }
                    else
                    {// code for IE6, IE5
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange=function()
                    {
                        if (xmlhttp.readyState==4 && xmlhttp.status==200)
                        {
                            document.getElementById("conteudo").innerHTML=xmlhttp.responseText;

                        }
                    }

                    xmlhttp.open('GET','getAllVideos.php?pagina='+pagina,true);


                    xmlhttp.send();
                }

            </script>


            <div class="row" >

                <div class="col-md-5 column">

                </div>

                <div class="col-md-7 column" >
                    <button style="padding: 0;border: none;background: none;" onclick="prevPage()"><img src="img/seter.png"></button>
                    <span id="page" onchange="funcao_ajax(this)">1</span>
                    <button style="padding: 0;border: none;background: none;" onclick="nextPage()"><img src="img/setdr.png"></button>
                </div>

            </div>

        </div>
    </div>

    <!--<input type="submit" onclick="getVideos()">-->



    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
                autoPlay:true,
                navigation : false, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem:true,
                pagination:false,
                scrollPerPage : false,
                paginationNumbers: false,

                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    </script>


    <!--
<div style="text-align:center;">
       <nav>
        <?php

    //echo "
    //          <ul class='pagination'>
    //           <li>
    //
    //             <a href='?pagina=".($_GET['pagina']-1)."' aria-label='Previous' id='setaPageEsquerda' onclick='getVideos()''>
    //             <img src='img/seteb.png' alt=''>
    //         </a>
    //     </li>
    //   <li><a href='?pagina=".$_GET['pagina']."' style='font-size:1.9em;' onclick='getVideos()'>".($_GET['pagina'])."</a></li>
    //
    //<li>
    //<a href='?pagina=".($_GET['pagina']+1)."' aria-label='Next' id='setaPageDireita' onclick='getVideos()'>
    //<img src='img/setdr.png' alt=''>
    //</a>
    //</li>
    //</ul>";
    ?>


       </nav>
     </div>-->

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">

    <!-- Default Theme -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">

    <!--  jQuery 1.7+  -->
    <script src="owl/assets/js/jquery-1.9.1.min.js"></script>

    <!-- Include js plugin -->
    <script src="owl/owl-carousel/owl.carousel.js"></script>

<?php include_once "footer_project.php";?>