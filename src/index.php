<?php
	include_once "header.php";
	include_once "functions.php";
?>
	
<!--<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<img src="img/nepopscontrucao.png" alt="" width="300em;">

			<br>

			

			<h1 style="color: #5b62ab;">Esse site está em construção.</h1>

			

			<h4 style="color: #5b62ab;">Retorne em breve para conhecer os trabalhos desenvolvidos pelo Núcleo de Educação Pupular
			da Paraíba. </h4>
		</div>
	</div>
</div>-->





<div class="row clearfix" id="containerCar">
		<div class="col-md-6 column">
			<div id="owl-demo" class="owl-carousel owl-theme">
				<?php $home = getHomeNepops();
				echo "<div class='item imgPrin'><a href='".$home[0]->link1."'><img src='data:image/jpeg;base64,".base64_encode($home[0]->imagem1)."' alt='The Last of us' class='widthandHeight'></a></div>"
				?>
			</div>
		</div>
		<div class="col-md-6 column" id="img_lateral">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div id="owl-demo2" class="owl-carousel owl-theme">
				<?php


				echo "<div class='item imgPrin2'><a href='".$home[0]->link2."'><img src='data:image/jpeg;base64,".base64_encode($home[0]->imagem2)."' alt='The Last of us' class='widthandHeight'></a></div>";
				?>

			</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-12 column">
					<div id="owl-demo3" class="owl-carousel owl-theme">
 				<?php
				echo "<div class='item imgPrin2'><a href='".$home[0]->link3."'><img src='data:image/jpeg;base64,".base64_encode($home[0]->imagem3)."' alt='The Last of us' class='widthandHeight'></a></div>";
				?>
			</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row clearfix" id="containerNot">
		<div class="col-md-12 column">
			<?php

			$noticias = getNotificias();

				$i = 0;
			$qnt = count($noticias);
				foreach($noticias as $key => $valor){
					echo "<div class='row clearfix'>";
						echo "<div class='col-md-4 column imgProjetosNoticias'>";
						echo "<img alt='140x140' src='data:image/jpeg;base64,".base64_encode($valor->imagem)."' class='widthandHeight'>";
						
						echo "</div>";
						echo	"<div class='col-md-8 column'>";
						echo		"<a href='noticia_projeto.php?id=".$valor->projeto_id."&noticiaid=".$valor->id."'><h2 class='title' style='margin-top: 0px' data-lightbox='image-1'>";
						echo			$valor->titulo;
						echo		"</h2></a>";
						echo		"<p>";
						echo			$valor->descricao;
						echo		"</p>";

						echo	"</div>";
					echo "</div>";

					$i++;

					if($i < $qnt ){
						echo "<hr>";
					}

				}

			?>

		</div>

	</div>
	<script>
    $(document).ready(function() {

      $("#owl-demo").owlCarousel({
          autoPlay:false,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:true,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1,
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false

      });

});
    $(document).ready(function() {

      $("#owl-demo2").owlCarousel({
          autoPlay:false,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1,
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false

      });

});
    $(document).ready(function() {

      $("#owl-demo3").owlCarousel({
          autoPlay:false,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1,
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false

      });

});
  </script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">

  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">

  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>

  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>


<?php include_once "footer_project.php"; ?>