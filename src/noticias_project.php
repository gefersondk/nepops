<?php include_once "header_project.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral"><b><?php echo $projeto[0]->sigla;?></b></span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<h3><span id="titulo_noticia">NOTÍCIAS</span></h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
							<li class="list-group-item"><a href="projeto.php?id=<?php echo $projeto[0]->id;?>">Apresentação</a></li>
							<li class="list-group-item" style="background-color:#5b62ab;"><a style="color:white;" href="noticias_project.php?id=<?php echo $projeto[0]->id;?>" >Notícias</a></li>
							<li class="list-group-item"><a href="biblioteca_project.php?id=<?php echo $projeto[0]->id;?>">Biblioteca</a></li>
							<li class="list-group-item"><a href="videos_project.php?id=<?php echo $projeto[0]->id;?>">Vídeos</a></li>
							<li class="list-group-item"><a href="photos_project.php?id=<?php echo $projeto[0]->id;?>">Fotos</a></li>
							<li class="list-group-item"><a href="contato_project.php?id=<?php echo $projeto[0]->id;?>">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>

		<?php
		try{
			include_once "functions.php";


		if(isset($_GET['id'])) {
			$id = $_GET['id'];
			$noticias = getAllNotificiasId($id);

			echo "<div class='col-md-9 column'>";
			if($noticias) {
				foreach ($noticias as $key => $value) {

					echo "<div class='row clearfix' style='margin-bottom: 1.2em;'>";
					echo "<div class='col-md-6 column imgProjetos'>";
					echo "<img alt='140x140' src='data:image/jpeg;base64," . base64_encode($value->imagem) . "' class='widthandHeight' />";
					echo "</div>";
					echo "<div class='col-md-6 column'>";
					echo "<h3 class='titleProject' style='margin-top: 0em'><a href='noticia_projeto.php?id=" . $value->projeto_id . "&noticiaid=". $value->id ."'><span id='tituloProjeto' style='font-family: AvenirNextLTPro-MediumCn;font-size:1.2em;text-indent: 5em;'>" . $value->titulo . "</span></a></br>";
					echo "<p style='color: #000000'><span id='descricaoProjeto'>" . $value->descricao . "</span></p>";
					echo "</div>";
					echo "</div>";


				}
			}else{
				echo "Este projeto não possui noticias.<br>";
			}
			echo "</div>";
		}
		}catch(Exception $e){
			echo "Erro ao listar os projetos.";

		}

		?>

	</div>
	


<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>


<?php include_once "footer_project.php"; ?>