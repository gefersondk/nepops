<?php include_once "header_project.php"; include_once "functions.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">

			<h3><span id="cabecalho_menu_lateral"><b><?php echo $projeto[0]->sigla;?></span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<?php
				if(isset($_GET["noticiaid"])) {
					$id = $_GET["noticiaid"];
					$apresentacao = getApresentacaoNoticia($id);
				}else{
					//TODO redirect
				}

			?>
			<h3><span id="titulo_noticia"><?php echo $apresentacao[0]->titulo;?></span></h3>
		</div>
	</div>
			
			<!--apagar e descomentar o dinamico //echo-->
			<div class='row clearfix'>
							<div class='col-md-3 column'>
							<div class='list-group'>
				<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
							<li class="list-group-item"><a href="projeto.php?id=<?php echo $projeto[0]->id;?>">Apresentação</a></li>
							<li class="list-group-item" style="background-color:#5b62ab;"><a style="color:white;" href="noticias_project.php?id=<?php echo $projeto[0]->id;?>" >Notícias</a></li>
							<li class="list-group-item"><a href="biblioteca_project.php?id=<?php echo $projeto[0]->id;?>">Biblioteca</a></li>
							<li class="list-group-item"><a href="videos_project.php?id=<?php echo $projeto[0]->id;?>">Vídeos</a></li>
							<li class="list-group-item"><a href="photos_project.php?id=<?php echo $projeto[0]->id;?>">Fotos</a></li>
							<li class="list-group-item"><a href="contato_project.php?id=<?php echo $projeto[0]->id;?>">Contatos</a></li>
						</ul>
					</p>
			<!--apagar e descomentar o dinamico //echo aqui termina-->
				
				<?php	

					try{


						if(isset($_GET["id"])){
							$id = $_GET["id"];

							echo	"</div>";
							echo	"</div>";
							echo	"<div class='col-md-9 column'>";



							echo "<table style='float:left;'>";
							echo "<tr><td style='padding-right: 6%;'><div id='imgProjetoLogo'><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->imagem)."' alt='' class='widthandHeight' ></div></td></tr>";


								//print_r($apresentação);
							//echo "<table style='float:left;'>";
							//echo	"<tr><td><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->logo)."' alt='' style='padding-right: 6%;'></td><td></td></tr>";
							echo	"<tr><td><h6><span id='descricao_noticia' ></span></h6></td></tr>";
							echo "</table>";
				
				
							echo "<span style='font-family: AvenirLTStdMedium;font-size:1em;text-indent: 5em;'>";
							echo "<p>".$apresentacao[0]->noticia."</p></span>";

						}else{
							//TODO redirect
						}

					}catch(Exception $e){
						echo "Não foi possível encontrar o projeto.";
					}
					
				?>

			</span>
		</div>
	</div>
	


<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>

<script>
var flip = 0;
/*$( "button" ).click(function() {
  $( "#continueText" ).toggle( flip++ % 2 == 0);
});*/

// Hide the extra content initially, using JS so that if JS is disabled, no problemo:
$('.continueText').addClass('hide')
$('.read-more-show, .read-more-hide').removeClass('hide')

// Set up the toggle effect:
$('.read-more-show').on('click', function(e) {
  $(this).next('.continueText').removeClass('hide');
  $(this).addClass('hide');
  e.preventDefault();
});

// Changes contributed by @diego-rzg
$('.read-more-hide').on('click', function(e) {
  var p = $(this).parent('.continueText');
  p.addClass('hide');
  p.prev('.read-more-show').removeClass('hide'); // Hide only the preceding "Read More"
  e.preventDefault();
});

</script>

<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>

<?php include_once "footer_project.php"; ?>