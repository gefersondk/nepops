<?php include_once "header_project.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral"><b><?php echo $projeto[0]->sigla?></b></span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<h3><span id="titulo_noticia">Contato</span></h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">


							<li class="list-group-item"><a href="projeto.php?id=<?php echo $projeto[0]->id;?>">Apresentação</a></li>
							<li class="list-group-item" ><a  href="noticias_project.php?id=<?php echo $projeto[0]->id;?>" >Notícias</a></li>
							<li class="list-group-item"><a href="biblioteca_project.php?id=<?php echo $projeto[0]->id;?>">Biblioteca</a></li>
							<li class="list-group-item"><a href="videos_project.php?id=<?php echo $projeto[0]->id;?>">Vídeos</a></li>
							<li class="list-group-item"><a href="photos_project.php?id=<?php echo $projeto[0]->id;?>">Fotos</a></li>
							<li class="list-group-item" style="background-color:#5b62ab;"><a style="color:white;" href="contato_project.php?id=<?php echo $projeto[0]->id;?>">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>
		
		<div class="col-md-9 column">
			
			<!--<table style="float:left;">
				<tr><td><img src="https://worldgastro.files.wordpress.com/2012/06/seu-zc3a9.jpg" alt="" style="padding-right: 6%;"></td><td></td></tr>
				<tr><td><h6><span id="descricao_noticia" >Seu zé Barnabé, Alisson é</span></h6></td></tr>
			</table>-->
			
			<div class="row">
				<div class="col-md-3">
				</div>
				<div class="col-md-6">
					<form role="form" id="formContact" method="POST" action="http://nepopspb.com/send_email.php">
					<div class="form-group">
							 
							<label for="nomeContato">
								<span class="cor_font">Nome</span>
							</label>
							<input type="text" class="form-control" id="nomeContato" name="name" required/>
						</div>
						<div class="form-group">
							 
							<label for="emailContato">
								<span class="cor_font">Email</span>
							</label>
							<input type="email" class="form-control" id="emailContato" name="email" required/>
						</div>
						<div class="form-group">
							 
							<label for="telefoneContato">
								<span class="cor_font">Telefone</span>
							</label>
							<input type="text" class="form-control" id="telefoneContato" name="telefone" />
						</div>

						<div class="form-group">
						    <label for="mensagemContato"><span class="cor_font">Mensagem</span></label>
						    <textarea class="form-control" rows="5" id="mensagemContato" required maxlength="500" name="mensagem"></textarea>
						</div>
						
						
						<button type="submit" class="btn btn-default pull-right">
							Enviar
						</button>
					</form>
				</div>
				<div class="col-md-3">
				</div>
			</div>
			
		</div>

	</div>
	


	<?php $rodapeDados = getRodape();
	 
		$string = $rodapeDados[0]->latitude;
		$count = 0;
		
		$patterns = array();
		$patterns[0] = '/width="[0-9]*"/';
		$patterns[1] = '/height="[0-9]*"/';
		$replacements = array();
		$replacements[2] = 'width="100%"';
		$replacements[1] = 'height="400px"';

		$maps = preg_replace($patterns, $replacements, $string, -1, $count);

	 ?>

	<div class="container-fluid" style="padding-top:5em;">
		<div class="row">
			<div class="col-md-12">
			<div id="maps"><?php echo $maps; ?></div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<ul class="list-inline">
					<li>
						<span class="cor_font">Telefone:</span> <span class="font_style"><?php echo $rodapeDados[0]->telefone; ?></span>
					</li>
					<li>
						<span class="cor_font">Email:</span> <span class="font_style"><?php echo $rodapeDados[0]->longitude; ?></span>
					</li>
					<li>
						<span class="cor_font">Endereço:</span> <span class="font_style"><?php echo $rodapeDados[0]->endereco; ?></span>
					</li>
				</ul>
			</div>
		</div>
	</div>

<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>


<?php include_once "footer_project.php"; ?>