<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Nepops</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

	
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
	<link rel="stylesheet" href="css/base-style.css">

  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   

  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   

  <script src="/media/owl/assets/js/jquery-1.9.1.min.js"></script>
   

  <script src="owl/owl-carousel/owl.carousel.js"></script>

  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-57-precomposed.png">
  <link rel="shortcut icon" href="img/favicon.png">
  
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/scripts.js"></script>

  <style>
	a{
		color: #5b62ab;
	}
	
	.imgSelect img{
		
	
		width: 5em;
		
	}

	#btTextContinue{
		background-color: none;
		border: none;
		color: #5b62ab;
		text-decoration: none;
		background: none;
	}
  </style>

</head>

<body>

<div class="container page-wrap" >
	<div class="row clearfix" style="padding-top:15px;">
		<div class="col-md-12 column">
			<div class="row clearfix">
				<?php include_once "functions.php";
					@$id = $_GET['id'];
					if(!isset($_GET['id'])){
						$id = 11;
					}

					//$logo = getLogoProject($id);
					//$title = getTitleProject($id);
					$projeto = getProjectId($id);
					echo "<div class='col-md-12 column'>";
					echo	"<img alt='140x140' src='data:image/jpeg;base64,".base64_encode($projeto[0]->logo)."' id='logo_project'>";

					echo	"<ul id='title_menu_project'>";
					echo		"<li><span id='title_project'>".$projeto[0]->sigla."</span></li>";
					echo		"<li><span id='subTitle_project'>".$projeto[0]->titulo."</span></li>";
					echo	"</ul>";
				?>
				<div class='column' id='div_lupa'>
					<script>
						function buscar(){
							document.getElementById("form").submit();
						}
					</script>
					<form id = "form" method="get" action="busca_noticias.php"><a href="#" onclick="buscar()" ><img src="img/lupa.png" alt="" id="lupa"></a><div id="paralelogramo"><input type="text" name="valor"></div></form>
				</div>
			</div>
				
		</div>
	</div>
</div>
	<div class="row clearfix" style="margin-bottom:20px;">
		<div class="col-md-12 column">
			<div class="row clearfix" >
				
				<div class="col-md-12 column" >
					<nav class="navbar navbar-default" role="navigation" id="style-menu">
						<div class="navbar-header hidden-md hidden-lg">
							 <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only"></span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="index.php">NEPOPS</a>
						</div>
						
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav style-menu-li paddingLeftMenuBar">
								<li>
									<a class="itensMenu" href="index.php" >HOME</a>

								</li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt=""></li>
								<li>
									<a class="itensMenu" href="historia.php">NEPOPS</a>
								</li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								<li><a class="itensMenu" href="projetos.php">PROJETOS</a></li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								<li><a class="itensMenu" href="noticias.php">NOTÍCIAS</a></li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								<li><a class="itensMenu" href="videos.php">VÍDEOS</a></li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								<li><a class="itensMenu" href="coletivos.php">COLETIVOS</a></li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								<li><a class="itensMenu" href="biblioteca.php">BIBLIOTECA</a></li>
								<li class="paddingSepareteMenuItens"><img src="img/barrinha_menu.png" alt="" style="display:inline-block;"></li>
								
							</ul>
							

						</div>
						
					</nav>
				</div>
				
			</div>
		</div>
	</div>