<?php include_once "header_project.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral"><?php echo $projeto[0]->sigla;?></b></span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<?php
			$imagens = getImagensPorAlbum($_GET['album_id']);
			$tituloAlbum = getAlbumImagensProjectId($_GET['id']);
			echo "<h3><span id='titulo_noticia'>".$tituloAlbum[0]->titulo."</span></h3>";
			?>

		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
					<li class="list-group-item"><a href="historia.php?id=11">História</a></li>
					<li class="list-group-item" ><a href="projetos.php" >Projetos</a></li>
					<li class="list-group-item"><a href="noticias.php">Notícias</a></li>
					<li class="list-group-item" ><a href="biblioteca.php" >Biblioteca</a></li>
					<li class="list-group-item" style="background-color:#5b62ab;"><a href="photos_nepops.php" style="color:white;">Fotos</a></li>
					<li class="list-group-item"><a href="videos.php">Vídeos</a></li>
					<li class="list-group-item"><a href="#">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>
		
		<div class="col-md-9 column" >
			
			<!--<table style="float:left;">
				<tr><td><img src="https://worldgastro.files.wordpress.com/2012/06/seu-zc3a9.jpg" alt="" style="padding-right: 6%;"></td><td></td></tr>
				<tr><td><h6><span id="descricao_noticia" >Seu zé Barnabé, Alisson é</span></h6></td></tr>
			</table>-->
			
			
			<span style="font-family: AvenirLTStdMedium;font-size:1.3em;" >

				<div id="owl-demo" class="imgSelect">

					<?php

					if(isset($_GET['id']) && isset($_GET['album_id'])){

						for($i =0;$i<sizeof($imagens);$i++){
							echo "<div class='item'><img src='data:image/jpeg;base64,".base64_encode($imagens[$i]->imagem)."' onclick='alterPage($i)' style='width:80px;height:80px'></div>";
						}

					}else{
						//TODO redirect
					}

					?>
				
				 
				</div>
			
			
				

				<br>
				<br>
				<br>
				<br>
				
				

			<div id="owl-demo2" class="owl-carousel owl-theme">



				<?php
				for($i =0;$i<sizeof($imagens);$i++){
					echo "<div class='item'><img data-src='data:image/jpeg;base64,".base64_encode($imagens[$i]->imagem)."'  class='lazyOwl' style='max-width:100%'></div>";
				}
				?>
			  
			 
			</div>
			
			
			<div class="row" >
			
				<div class="col-md-5 column">
				
				</div>
				
				<div class="col-md-7 column" >
					<button style="padding: 0;border: none;background: none;" onclick="prevPage()"><img src="img/seter.png"></img></button>
					
					<span id="page">1</span>
					<button style="padding: 0;border: none;background: none;" onclick="nextPage()"><img src="img/setdr.png"></img></button>
				</div>	
				
			</div>

				<br>

				<?php echo "<a href='photos_project.php?id=".$projeto[0]->id."'>Voltar para Albuns</a>"; ?>

			</span>
		</div>

	</div>
<script>

	function setPage(x){
		var p =  x + 1;
		$( "#page" ).html(p);
	}
	
</script>

<script>
		
	
    $(document).ready(function() {
	 qnt = 0;
	 owl = $("#owl-demo2");
     
      $("#owl-demo").owlCarousel({
     
          autoPlay: false, //Set AutoPlay to 3 seconds
     
          items : 9,
          //itemsDesktop : [1199,3],
          //itemsDesktopSmall : [979,3],
		  pagination: false,
		  paginationNumbers: false,
		  addClassActive:true
     
      });
	 
	  
	  owl.owlCarousel({
     
          autoPlay: false,     
          singleItem:true,
		  pagination: false,
		  mouseDrag:false,
		  lazyLoad: true,
		  rewindNav:false,
		  afterInit : countItens
		  
     	
      });
	  
	 function  countItens(){
		 qnt = this.owl.owlItems.length;
	 }
     
    });

	
	function alterPage(page){
		owl.trigger("owl.goTo",page);
		setPage(page);
		
	}
	
	function nextPage(){
		owl.trigger("owl.next");
		var page = parseInt($('#page').text());
		var count = qnt;
		
		if(page < count)
			setPage(page);
	}
	
	function prevPage(){
		owl.trigger("owl.prev");
		var page = parseInt($('#page').text());
		if(page > 1)
			setPage(page-2);
		
	}
	
</script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>


<?php include_once "footer_project.php"; ?>