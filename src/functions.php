<?php 
@session_start();
include_once 'conexao.php';

function getCountVideos(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select count(*) as count from Videos");
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetch(PDO::FETCH_OBJ);
        endif;
    } catch (PDOException $e) {
            echo "Erro ao contar os videos";
    }
}

function getCountBibliotecaDados(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select count(*) as count from Bibliotecas");
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetch(PDO::FETCH_OBJ);
        endif;
    } catch (PDOException $e) {
            echo "Erro ao contar os dados da biblioteca";
    }
}

function getCountVideosProject($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select count(*) as count from Videos WHERE projeto_id = :id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetch(PDO::FETCH_OBJ);
        endif;
    } catch (PDOException $e) {
            echo "Erro ao contar os videos";
    }
}

function getAllVideos($pagina=1){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("select descricao, url,titulo from Videos ORDER BY id DESC LIMIT ".(($pagina-1)*4)." ,4");
        $listar->execute();
     
        if($listar->rowCount()>0):
                return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
            
    } catch (PDOException $e) {
            echo $e;
            echo "Erro ao listar os Videos<br>";
    }
}


function getBibliotecasPaginacao($pagina=1){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("select *  FROM Bibliotecas ORDER BY id DESC LIMIT ".(($pagina-1)*4)." ,4");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar as bibliotecas<br>";
    }
}


function getAllVideosProjeto($pagina=1,$idProjeto){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select descricao, url,titulo from Videos where projeto_id = :id  LIMIT ".(($pagina-1)*4)." ,4");
        $listar->bindValue(":id",(int)$idProjeto);
        $listar->execute();

        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo $e;
        echo "Erro ao listar os Videos<br>";
    }
}

function getVideosProject($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select v.descricao,v.link from videos v join projetos_videos pv on pv.id_videos = v.id where pv.id_projetos = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
            
    } catch (PDOException $e) {
            echo "Erro ao listar as Imagens<br>";
    }
}


//getVideos nepops project
function getVideosProjectNepops($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select * FROM Videos WHERE projeto_id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
                return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;
            
    } catch (PDOException $e) {
            echo "Erro ao listar os videos.<br>";
    }
}


function teste(){
    $pdo=conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM  Imagem");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);
        
      
    
    } catch (PDOException $e) {
        echo "Erro ao listar os dados<br>";
    }
}


//retorna todos os projetos
/*function getProjects(){
    $pdo = conectarComPdo();
    try{
         $listar = $pdo->query("SELECT * FROM Projetos WHERE sigla not like 'NEPOPS'");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca projetos";
    }
}*/

function getProjects(){
    $pdo = conectarComPdo();
    try{
         $listar = $pdo->query("SELECT * FROM Projetos WHERE sigla not like 'NEPOPS' and status != 't'");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca projetos";
    }
}

//TODO
function getProjectId($id){
         $pdo = conectarComPdo();
        try{
            $listar = $pdo->prepare("select *  FROM Projetos WHERE id = :id");
            $listar->bindValue(":id",$id);//."%"
            $listar->execute();
            if($listar->rowCount()>0):
                    return $listar->fetchAll(PDO::FETCH_OBJ);
            endif;
                
        } catch (PDOException $e) {
                echo "Erro ao buscar  o projeto<br>";
        }
}

function getApresentacaoProjeto($id){
    $pdo = conectarComPdo();
        try{
            $listar = $pdo->prepare("select logo, textoApresentacao FROM Projetos WHERE id = :id");
            $listar->bindValue(":id",$id);//."%"
            $listar->execute();
            if($listar->rowCount()>0):
                    return $listar->fetchAll(PDO::FETCH_OBJ);
            endif;
                
        } catch (PDOException $e) {
                echo "Erro ao listar a apresentaçãod o projeto<br>";
        }
}

function getApresentacaoNoticia($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select * FROM Noticias WHERE id = :id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao listar a apresentaçãod o projeto<br>";
    }
}

function getCapaAlbumNoticia($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select * FROM Imagens WHERE album_id = :id LIMIT 1");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao pesquisar capa do album<br>";
    }
}

function getImagensPorAlbum($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select * FROM Imagens WHERE album_id = :id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao pesquisar capa do album<br>";
    }
}


function getLogoProject($id){
        $pdo = conectarComPdo();
        try{
            $listar = $pdo->prepare("select logo FROM Projetos WHERE id = :id");
            $listar->bindValue(":id",$id);//."%"
            $listar->execute();
            if($listar->rowCount()>0):
                    return $listar->fetchAll(PDO::FETCH_OBJ);
            endif;
                
        } catch (PDOException $e) {
                echo "Erro ao buscar logo do projeto<br>";
        }
}

function getColetivos(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT * FROM  Projetos WHERE status = 't'");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca projetos";
    }
}

function getHomeNepops(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT imagem1, imagem2, imagem3, link1, link2, link3 FROM  Home");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca Home";
    }
}

function getNotificias(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT id, projeto_id, imagem, titulo, descricao, data_publicacao FROM  Noticias WHERE data_publicacao <= CURDATE() ORDER BY data_criacao DESC LIMIT 3 ");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca noticias";
    }
}

function getAllNotificias($pagina){
    $pdo = conectarComPdo();
    try{
        $paginaInicial = $pagina*5;
        $paginaFinal = ($pagina*5)+5;
        $listar = $pdo->query("SELECT id,imagem, titulo, descricao,noticia, data_publicacao FROM  Noticias WHERE data_publicacao <= CURDATE() ORDER BY data_criacao DESC LIMIT ".$paginaInicial.",".$paginaFinal);
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca noticias";
    }
}

function getAllNoticiasBusca($pagina,$valor){
    $pdo = conectarComPdo();
    try{
        $paginaInicial = $pagina*5;
        $paginaFinal = ($pagina*5)+5;
        $listar = $pdo->query("SELECT id, projeto_id, imagem, titulo, descricao,noticia, data_publicacao FROM  Noticias WHERE data_publicacao <= CURDATE() and tags like '%".$valor."%' ORDER BY data_criacao DESC LIMIT ".$paginaInicial.",".$paginaFinal);
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca noticias";
    }
}





function getAllNotificiasId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("SELECT id, projeto_id, imagem, titulo, descricao,noticia, data_publicacao FROM  Noticias WHERE projeto_id = :id");
        $listar->bindValue(":id",$id);
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro busca noticias<br>";
    }
}

function getProjetosRodape(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("SELECT id, sigla, link FROM  Projetos WHERE sigla not like 'NEPOPS' ORDER BY sigla ASC LIMIT 16");
        $listar->execute();
        return $listar->fetchAll(PDO::FETCH_OBJ);


    }catch(PDOException $e){
        echo $e ."Erro busca projetos rodape";
    }
}

function getTextoProjeto($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select textoApresentacao as texto  FROM Projetos WHERE id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar  a biblioteca<br>";
    }
}


function getBibliotecaId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select *  FROM Bibliotecas WHERE projeto_id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar  a biblioteca<br>";
    }
}


function getBibliotecas(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("select *  FROM Bibliotecas");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar as bibliotecas<br>";
    }
}

function getRodape(){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->query("select *  FROM Rodape");
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar as bibliotecas<br>";
    }
}

function getImagensProjectId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select *  FROM Imagens WHERE Projeto_id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar  a imagens do projeto<br>";
    }
}

function getAlbumImagensProjectId($id){
    $pdo = conectarComPdo();
    try{
        $listar = $pdo->prepare("select *  FROM Album WHERE Projetos_id = :id");
        $listar->bindValue(":id",$id);//."%"
        $listar->execute();
        if($listar->rowCount()>0):
            return $listar->fetchAll(PDO::FETCH_OBJ);
        endif;

    } catch (PDOException $e) {
        echo "Erro ao buscar  a album das imagens do projeto<br>";
    }
}

//$v = teste();
//header('Content-Type: image/png');
//foreach ($v as $key) {
    //print_r($key);
   // print_r($key['imagem']);
    //echo "<pre>"; var_dump($key['imagem']); echo "</pre>";
    //echo  stream_get_contents($key['imagem']);

//}

//fpassthru($v[0]->imagem);
//print_r($t);
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";
//echo "<br>";


//$l=base64_encode((string)stream_get_contents($v[0]->imagem));
//print_r($l);

//echo "<img src='data:image/png;base64,".$l."' />";


//echo "oi<img src='C:\\Users\\Public\\Pictures\\Sample Pictures\\Koala.jpg'>";
//$a = imagens("D","Gefe");
//foreach ($a as $key) {
    //echo "<img src=".".".$key->imagem.".png"."."."></img>";
//}
?>