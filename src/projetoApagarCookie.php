<?php include_once "header_project.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral"><b>VEPOP-SUS</b></span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<h3><span id="titulo_noticia">Apresentação</span></h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
							<li class="list-group-item"  style="background-color:#5b62ab;"><a href=""  style="color:white;">Apresentação</a></li>
							<li class="list-group-item"><a href="noticias_project.php">Notícias</a></li>
							<li class="list-group-item"><a href="biblioteca_project.php">Biblioteca</a></li>
							<li class="list-group-item"><a href="videos_project.php">Vídeos</a></li>
							<li class="list-group-item"><a href="photos_project.php">Fotos</a></li>
							<li class="list-group-item"><a href="contato_project.php">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>
		<div class="col-md-9 column">
			
			
				
				<?php	

					try{

						include_once "functions.php";

						if(isset($_GET["id"])){
							$id = $_GET["id"];
							$apresentacao = getApresentacaoProjeto($id);
							$textTotal = strlen($apresentacao[0]->textoApresentacao);
							if($textTotal > 1500){
								//print_r($apresentação);
							echo "<table style='float:left;'>";
							echo	"<tr><td><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->logo)."' alt='' style='padding-right: 6%;'></td><td></td></tr>";
							echo	"<tr><td><h6><span id='descricao_noticia' ></span></h6></td></tr>";
							echo "</table>";
				
				
							echo "<span style='font-family: AvenirLTStdMedium;font-size:1em;text-indent: 5em;'>";
							//echo "<p>".$apresentacao[0]->textoApresentacao."</p>";
							echo "<p>".$texto = substr($apresentacao[0]->textoApresentacao,0 , 1500)."</p>";
							echo "<div style='display:none' id='continueText'>".$textoContinue = substr($apresentacao[0]->textoApresentacao, 1500, $textTotal)."</div>";

							//echo "<p>Hello</p>";
							//echo "<p >Good Bye</p>";
							echo "<button id='btTextContinue'>continuar lendo</button>";
							}else{

								//print_r($apresentação);
							echo "<table style='float:left;'>";
							echo	"<tr><td><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->logo)."' alt='' style='padding-right: 6%;'></td><td></td></tr>";
							echo	"<tr><td><h6><span id='descricao_noticia' ></span></h6></td></tr>";
							echo "</table>";
				
				
							echo "<span style='font-family: AvenirLTStdMedium;font-size:1em;text-indent: 5em;'>";
							echo "<p>".$apresentacao[0]->textoApresentacao."</p>";
							}
							
						}
					}catch(Exception $e){
						echo "Não foi possível encontrar o projeto.";
					}
					
				?>

			</span>
		</div>
	</div>
	


<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>

<script>
var flip = 0;
$( "button" ).click(function() {
  $( "#continueText" ).toggle( flip++ % 2 === 0 );
});
</script>

<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>

<?php include_once "footer_project.php"; ?>