CREATE DATABASE NEPOPS;
USE	NEPOPS;
CREATE TABLE NEPOPS_HISTORICO(ID INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(ID));
CREATE TABLE NEPOPS_HISTORICO_IMAGEM(ID_NEPOPS INT NOT NULL, ID_IMAGEM INT NOT NULL,PRIMARY KEY(ID_NEPOPS,ID_IMAGEM));
CREATE TABLE IMAGEM(ID INT NOT NULL AUTO_INCREMENT, LINK VARCHAR(255) NOT NULL, PRIMARY KEY(ID));

ALTER TABLE NEPOPS_HISTORICO_IMAGEM
ADD CONSTRAINT NEPOPS_HISTORICO_FK 
FOREIGN KEY	(ID_NEPOPS) REFERENCES NEPOPS_HISTORICO (ID);

ALTER TABLE NEPOPS_HISTORICO_IMAGEM
ADD CONSTRAINT IMAGEM_FK 
FOREIGN KEY	(ID_IMAGEM) REFERENCES IMAGEM (ID);

CREATE TABLE NEPOPS_FOTOS(ID INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(ID));
CREATE TABLE NEPOPS_FOTOS_IMAGEM(ID_NEPOPS INT NOT NULL, ID_IMAGEM INT NOT NULL,PRIMARY KEY(ID_NEPOPS,ID_IMAGEM));

ALTER TABLE NEPOPS_FOTOS_IMAGEM
ADD CONSTRAINT NEPOPS_FOTOS_FK 
FOREIGN KEY	(ID_NEPOPS) REFERENCES NEPOPS_HISTORICO (ID);

ALTER TABLE NEPOPS_FOTOS_IMAGEM
ADD CONSTRAINT IMAGEM2_FK 
FOREIGN KEY	(ID_IMAGEM) REFERENCES IMAGEM (ID);

CREATE TABLE DESCRICAO(ID INT NOT NULL AUTO_INCREMENT, TEXTO LONGTEXT,PRIMARY KEY(ID));
CREATE TABLE NEPOPS_HISTORICO_DESCRICAO(ID_NEPOPS INT NOT NULL, ID_DESCRICAO INT NOT NULL,PRIMARY KEY(ID_NEPOPS,ID_DESCRICAO));

ALTER TABLE NEPOPS_HISTORICO_DESCRICAO
ADD CONSTRAINT NEPOPS_HISTORICO2_FK
FOREIGN KEY (ID_NEPOPS) REFERENCES NEPOPS_HISTORICO(ID);

ALTER TABLE NEPOPS_HISTORICO_DESCRICAO
ADD CONSTRAINT DESCRICAO_FK
FOREIGN KEY (ID_DESCRICAO) REFERENCES DESCRICAO(ID);



CREATE TABLE VIDEOS(ID INT NOT NULL AUTO_INCREMENT, TITULO VARCHAR (255), LINK VARCHAR(255) NOT NULL, PRIMARY KEY(ID));

CREATE TABLE PROJETOS(ID INT NOT NULL AUTO_INCREMENT, TITULO VARCHAR(50) NOT NULL, SUBTITULO VARCHAR(50) NOT NULL, LOGO INT, LINK VARCHAR(255) NOT NULL,PRIMARY KEY(ID));

CREATE TABLE PROJETOS_DESCRICAO(ID_PROJETOS INT NOT NULL, ID_DESCRICAO INT NOT NULL, PRIMARY KEY(ID_PROJETOS,ID_DESCRICAO));

ALTER TABLE PROJETOS
ADD CONSTRAINT PROJETOS_LOGO_FK
FOREIGN KEY (LOGO) REFERENCES IMAGEM(ID);

ALTER TABLE PROJETOS_DESCRICAO
ADD CONSTRAINT PROJETOS_FK
FOREIGN KEY (ID_PROJETOS) REFERENCES PROJETOS(ID);

ALTER TABLE PROJETOS_DESCRICAO
ADD CONSTRAINT DESCRICAO2_FK
FOREIGN KEY (ID_DESCRICAO) REFERENCES DESCRICAO(ID);


CREATE TABLE PROJETOS_IMAGEM(ID_PROJETOS INT NOT NULL, ID_IMAGEM INT NOT NULL, PRIMARY KEY(ID_PROJETOS,ID_IMAGEM));

ALTER TABLE PROJETOS_IMAGEM
ADD CONSTRAINT PROJETOS2_FK
FOREIGN KEY (ID_PROJETOS) REFERENCES PROJETOS(ID);

ALTER TABLE PROJETOS_IMAGEM
ADD CONSTRAINT IMAGEM3_FK
FOREIGN KEY (ID_IMAGEM) REFERENCES IMAGEM(ID);

CREATE TABLE PROJETOS_VIDEOS(ID_PROJETOS INT NOT NULL, ID_VIDEOS INT NOT NULL, PRIMARY KEY(ID_PROJETOS,ID_VIDEOS));

ALTER TABLE PROJETOS_VIDEOS
ADD CONSTRAINT PROJETOS3_FK
FOREIGN KEY (ID_PROJETOS) REFERENCES PROJETOS(ID);

ALTER TABLE PROJETOS_VIDEOS
ADD CONSTRAINT VIDEOS_FK
FOREIGN KEY (ID_VIDEOS) REFERENCES VIDEOS(ID);

CREATE TABLE NOTICIAS(ID INT NOT NULL AUTO_INCREMENT,TITULO VARCHAR(255) NOT NULL, DATA DATE, PRIMARY KEY(ID));

CREATE TABLE NOTICIAS_DESCRICAO(ID_NOTICIAS INT NOT NULL, ID_DESCRICAO INT NOT NULL, PRIMARY KEY(ID_NOTICIAS,ID_DESCRICAO));

ALTER TABLE NOTICIAS_DESCRICAO
ADD CONSTRAINT NOTICIAS_FK
FOREIGN KEY (ID_NOTICIAS) REFERENCES NOTICIAS(ID);

ALTER TABLE NOTICIAS_DESCRICAO
ADD CONSTRAINT DESCRICAO3_FK
FOREIGN KEY (ID_DESCRICAO) REFERENCES DESCRICAO(ID);


CREATE TABLE NOTICIAS_IMAGEM(ID_NOTICIAS INT NOT NULL, ID_IMAGEM INT NOT NULL, PRIMARY KEY(ID_NOTICIAS,ID_IMAGEM));

ALTER TABLE NOTICIAS_IMAGEM
ADD CONSTRAINT NOTICIAS2_FK
FOREIGN KEY (ID_NOTICIAS) REFERENCES NOTICIAS(ID);

ALTER TABLE NOTICIAS_IMAGEM
ADD CONSTRAINT IMAGEM4_FK
FOREIGN KEY (ID_IMAGEM) REFERENCES IMAGEM(ID);

CREATE TABLE NOTICIAS_VIDEOS(ID_NOTICIAS INT NOT NULL, ID_VIDEOS INT NOT NULL, PRIMARY KEY(ID_NOTICIAS,ID_VIDEOS));

ALTER TABLE NOTICIAS_VIDEOS
ADD CONSTRAINT NOTICIAS3_FK
FOREIGN KEY (ID_NOTICIAS) REFERENCES NOTICIAS(ID);

ALTER TABLE NOTICIAS_VIDEOS
ADD CONSTRAINT VIDEOS2_FK
FOREIGN KEY (ID_VIDEOS) REFERENCES VIDEOS(ID);

CREATE TABLE USUARIO(ID INT NOT NULL AUTO_INCREMENT, NOME VARCHAR(50)NOT NULL, DATA_CRIACAO DATE NOT NULL, LOGIN VARCHAR(15) NOT NULL, SENHA VARCHAR(15) NOT NULL, EMAIL VARCHAR(50) NOT NULL, RESPOSTA_SECRETA VARCHAR(50) NOT NULL, TIPO INT NOT NULL, PRIMARY KEY(ID));

CREATE TABLE PROJETO_USUARIO(ID_PROJETO INT NOT NULL, ID_USUARIO INT NOT NULL, PRIMARY KEY(ID_PROJETO,ID_USUARIO));

ALTER TABLE PROJETO_USUARIO
ADD CONSTRAINT PROJETO4_FK
FOREIGN KEY (ID_PROJETO) REFERENCES PROJETOS(ID);

ALTER TABLE PROJETO_USUARIO
ADD CONSTRAINT USUARIO_FK
FOREIGN KEY (ID_USUARIO) REFERENCES USUARIO(ID);

CREATE TABLE LOG(ID INT NOT NULL AUTO_INCREMENT, ACAO VARCHAR(50), DATA DATE, ID_USUARIO INT NOT NULL ,PRIMARY KEY(ID));

ALTER TABLE LOG 
ADD CONSTRAINT USUARIO1_FK
FOREIGN KEY	(ID_USUARIO) REFERENCES USUARIO(ID);
