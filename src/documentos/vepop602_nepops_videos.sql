-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 216.172.161.50    Database: vepop602_nepops
-- ------------------------------------------------------
-- Server version	5.5.40-36.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'PK',
  `descricao` text CHARACTER SET utf8 COLLATE utf8_unicode_ci COMMENT 'Descrição dos Vídeos',
  `link` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Link do Vídeo',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1 COMMENT='Todos os videos do sistema NEPOPS';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
INSERT INTO `videos` VALUES (2,'Educação à Distância - Seminário Nacional de Educação Popular na Formação em Saúde','https://www.youtube.com/watch?v=rK9vMb0bLkI'),(3,'Oficina de Planejamento: Seminário de Educação Popular e Formação na Área de Saúde','https://www.youtube.com/watch?v=1e8PJa51gNw'),(4,'Educação Popular nos Cursos de Especialização em Regime de Residência','https://www.youtube.com/watch?v=8wEI5AriN6U'),(5,'Controle Social - Seminário Nacional de Educação Popular na Formação em Saúde','https://www.youtube.com/watch?v=rBjSG0aODLg'),(6,'Graduação - Seminário Nacional de Educação Popular na Formação em Saúde','https://www.youtube.com/watch?v=G-P0iCHLvq0'),(7,'FÓRUM PERMANENTE DE EDUCAÇÃO POPULAR EM SAÚDE DA PARAÍBA','https://www.youtube.com/watch?v=elyWZUg7Vd0'),(8,'Meio ambiente e Educação Popular/ 9º Fórum Permanente de Educação Popular em Saúde da Paraíba','https://www.youtube.com/watch?v=tPXXFLzWZEo'),(9,'Pensando a Participação Popular nas cidades - MOPS, ANEPS e PVP Paraíba','https://www.youtube.com/watch?v=EXek2VCg-gk'),(10,'Vivência Acampamento Wanderley Caixe - Caaporã','https://www.youtube.com/watch?v=x5qoPMxJs40'),(11,'Conhecendo sujeitos de movimentos e práticas populares de saúde da Paraíba','https://www.youtube.com/watch?v=6TkyPJFt3mM'),(12,'Reunião MOPS, ANEPS e ANEPOP','https://www.youtube.com/watch?v=TTWJGcwt9_U'),(13,'Retrospectiva MOPS e ANEPS do ano de 2014','https://www.youtube.com/watch?v=_eiHISyoBGU'),(14,'Vivencia na Unidade Demonstrativa Pegagógica de Agricultura Familiar da AGEMTE (versão longa)','https://www.youtube.com/watch?v=ToxI-J-nfmk'),(16,'Vivencia na Unidade Demonstrativa Pegagógica de Agricultura Familiar da AGEMTE','https://www.youtube.com/watch?v=ToxI-J-nfmk'),(17,'Promoção da leitura por meio da Biblioteca Popular - Curso de Extensão Popular Teoria e Prática','https://www.youtube.com/watch?v=RFj1UVkOssQ'),(18,'Curso Extensão Popular: teoria e prática - Extensão Popular em comunidades quilombolas','https://www.youtube.com/watch?v=kaQF3AyNxmM'),(19,'Educação Popular na Formação - Curso de Extensão Popular','https://www.youtube.com/watch?v=J0UOO4p9eD0'),(20,'Curso de Extensão: Movimentos Populares e Práticas Sociais Emancipadoras','https://www.youtube.com/watch?v=BXPjynCqoZ0'),(21,'Educação Popular','https://www.youtube.com/watch?v=tPXXFLzWZEo');
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-28 17:42:14
