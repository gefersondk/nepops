-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.8.0
-- PostgreSQL version: 9.4
-- Project Site: pgmodeler.com.br
-- Model Author: ---


-- Database creation must be done outside an multicommand file.
-- These commands were put in this file only for convenience.
-- -- object: novo_banco_de_dados | type: DATABASE --
-- -- DROP DATABASE IF EXISTS novo_banco_de_dados;
-- CREATE DATABASE novo_banco_de_dados
-- ;
-- -- ddl-end --
-- 

-- object: nepops | type: SCHEMA --
-- DROP SCHEMA IF EXISTS nepops CASCADE;
CREATE SCHEMA nepops;
-- ddl-end --
ALTER SCHEMA nepops OWNER TO postgres;
-- ddl-end --
COMMENT ON SCHEMA nepops IS 'Banco do Sistema Nepops';
-- ddl-end --

SET search_path TO pg_catalog,public,nepops;
-- ddl-end --

-- object: nepops.seq_videos | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS nepops.seq_videos CASCADE;
CREATE SEQUENCE nepops.seq_videos
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE nepops.seq_videos OWNER TO postgres;
-- ddl-end --

-- object: nepops.videos | type: TABLE --
-- DROP TABLE IF EXISTS nepops.videos CASCADE;
CREATE TABLE nepops.videos(
	id smallint NOT NULL DEFAULT nextval('nepops.seq_videos'::regclass),
	descricao text,
	link text NOT NULL,
	CONSTRAINT pk_videos PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE nepops.videos IS 'Tabela que armazena todos os videos do sistema nepops';
-- ddl-end --
COMMENT ON COLUMN nepops.videos.descricao IS 'Descrção do Video';
-- ddl-end --
COMMENT ON COLUMN nepops.videos.link IS 'Link do Video';
-- ddl-end --
ALTER TABLE nepops.videos OWNER TO postgres;
-- ddl-end --


