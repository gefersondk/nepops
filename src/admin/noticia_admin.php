<?php
	include_once("header_admin.php")
  ?>

<script src="js/tinymce/tinymce.min.js"></script>
<script>tinymce.init({
	language : 'pt_BR',
	selector:'textarea',
	menubar : false,
});</script>

<div class="container">
	<div class="row clearfix">
		<div class="col-md-6 column">
			<form class="form-horizontal" role="form">
				<div class="form-group">
					 <label for="inputEmail3" class="col-sm-2 control-label">Título</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputTitulo" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-2 control-label">Descrição</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="inputsubTtitulo" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						 <button type="submit" class="btn btn-default">Sign in</button>
					</div>
				</div>
			</form>
		</div>
		<div class="col-md-6 column">
		</div>
	</div>
</div>

<textarea>Easy! You should check out MoxieManager!</textarea>
</body>
</html>