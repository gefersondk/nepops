</div><!-- Fechando o Container principal-->

<div class="container-fluid" id="footer-main">

	<!--inicio dados-->
	<div class="col-md-12" id="style-footer-nepops-dados">
		<div class="row">

			<div class="col-md-12">
				<div class="row">
					<div class="col-md-3">
					</div>
					<div class="col-md-6">
						<div class="row">
							<?php
							$infoRodape = getProjetosRodape();
							$qntProjetos = count($infoRodape);
							$i = 0;
							for($j=0;$j<4;$j++){
								echo "<div class='col-md-3'>";
									echo "<ul>";
										for($k=0;$k<4;$k++){
											echo  ($i < $qntProjetos)? "<li><a href='projeto.php?id=".$infoRodape[$i]->id."'>".$infoRodape[$i++]->sigla."</a></li>" : "<li></li>";
										}
									echo "</ul>";
								echo "</div>";
							}

							?>
						</div>
					</div>
					<div class="col-md-3">
					</div>
				</div>
			</div>

		</div>
	</div>

	<!--fim dados-->

	<!--inicio imagens-->

	<div class="col-md-12" id="style-footer-nepops-img">
			<div class="row">
				<div class="col-md-2">
				</div>
				<div class="col-md-8">
					<div class="row">
						<div class="col-md-2">
							<img alt="140x140" src="img/logo_preto.png">
						</div>
						<div class="col-md-2">
							<img alt="140x140" src="img/anepop.png">
						</div>
						<div class="col-md-2">
							<img alt="140x140" src="img/ufpb.png">
						</div>
						<div class="col-md-2">
							<img alt="140x140" src="img/ministerioSaude.png">
						</div>
						<div class="col-md-2">
							<img alt="140x140" src="img/governo.png">
						</div>
						<div class="col-md-2">
							<img alt="140x140" src="img/ministerioEducacao.png">
						</div>
					</div>
				</div>
				<div class="col-md-2">
				</div>
			</div>
		</div>
	<!--fim imagens-->
</div>

</footer>
	

	




</body>
</html>
