<?php include_once "header.php";?>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral">NEPOPS</span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<h3><span id="titulo_noticia">NOTÍCIAS</span></h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
							<li class="list-group-item"><a href="historia.php?id=11">História</a></li>
							<li class="list-group-item"><a href="projetos.php">Projetos</a></li>
							<li class="list-group-item" style="background-color:#5b62ab;"><a href="noticias.php" style="color:white;">Notícias</a></li>
							<li class="list-group-item"><a href="biblioteca.php">Biblioteca</a></li>
							<li class="list-group-item"><a href="photos_nepops.php">Fotos</a></li>
							<li class="list-group-item"><a href="videos.php">Vídeos</a></li>
							<li class="list-group-item"><a href="#">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>

		<?php
		try{
			include_once "functions.php";

			$noticias = getAllNotificias(0);

			echo "<div class='col-md-9 column'>";

			foreach ($noticias as $key => $value) {

				echo "<div class='row clearfix' style='margin-bottom: 1.2em;'>";
				echo "<div class='col-md-6 column imgProjetos'>";
				echo "<img alt='140x140' src='data:image/jpeg;base64,".base64_encode($value->imagem)."' class='widthandHeight' />";
				echo "</div>";
				echo "<div class='col-md-6 column'>";
				echo "<h3 class='titleProject' style='margin-top: 0em'><a href='noticia_projeto.php?id=".$value->id."'><span id='tituloProjeto' style='font-family: AvenirNextLTPro-MediumCn;font-size:1.2em;text-indent: 5em;'>".$value->titulo."</span></a></br>";
				echo "<p style='color: #000000'><span id='descricaoProjeto'>".$value->descricao."</span></p>";
				echo "</div>";
				echo "</div>";


			}
			echo "</div>";
		}catch(Exception $e){
			echo "Erro ao listar os projetos.";

		}

		?>







	</div>
	


<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>



<?php include_once "footer_project.php"; ?>
