<?php
header('Content-Type: text/html; charset=utf-8');
include_once "header_project.php";

include_once "functions.php";


?>



    <div class="row clearfix">
        <div class="col-md-3 column">
            <h3><span id="cabecalho_menu_lateral">NEPOPS</span></h3>
            <hr>
        </div>
        <div class="col-md-9 column">
            <h3><span id="titulo_noticia" >Fotos</span></h3>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-md-3 column">
            <div class="list-group">


                <p class="list-group-item-text">
                <ul class="list-group" id="estilo_menu_lateral">
                    <li class="list-group-item"><a href="historia.php?id=11">História</a></li>
                    <li class="list-group-item" ><a href="projetos.php" >Projetos</a></li>
                    <li class="list-group-item"><a href="noticias.php">Notícias</a></li>
                    <li class="list-group-item" ><a href="biblioteca.php" >Biblioteca</a></li>
                    <li class="list-group-item" style="background-color:#5b62ab;"><a href="photos_nepops.php" style="color:white;">Fotos</a></li>
                    <li class="list-group-item"><a href="videos.php">Vídeos</a></li>
                    <li class="list-group-item"><a href="#">Contatos</a></li>
                </ul>
                </p>


            </div>
        </div>
        <div class="col-md-9 column" id="dados">
            <?php

            $album = getAlbumImagensProjectId(11);

            if($album){

                foreach($album as $key=> $value){
                    $capa = getCapaAlbumNoticia($value->id);
                    echo "<div class='row clearfix' style='margin-bottom: 1.2em;'>";
                    echo "<div class='col-md-6 column'>";
                    echo "<a href= 'photos_project_album_nepops.php?album_id=".$value->id."&id=".$projeto[0]->id."'><img alt='420x251' src='data:image/jpeg;base64,".base64_encode($capa[0]->imagem)."' width='420' height='251'></a>";//"<img alt='140x140' src='data:image/jpeg;base64," . base64_encode($value->imagem) . "' />"
                    echo "</div>";
                    echo "<div class='col-md-6 column'><h3  class='titleProject'><a href='photos_project_album_nepops.php?album_id=".$value->id."&id=".$projeto[0]->id."'>".$value->titulo."</a></h3></div>";
                    echo "</div>";
                }
            }
            ?>
        </div>
    </div>

    <!--<input type="submit" onclick="getVideos()">-->



    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
                autoPlay:true,
                navigation : false, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem:true,
                pagination:false,
                scrollPerPage : false,
                paginationNumbers: false,

                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    </script>


    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">

    <!-- Default Theme -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">

    <!--  jQuery 1.7+  -->
    <script src="owl/assets/js/jquery-1.9.1.min.js"></script>

    <!-- Include js plugin -->
    <script src="owl/owl-carousel/owl.carousel.js"></script>

<?php include_once "footer_project.php";?>