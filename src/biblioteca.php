<?php include_once "header.php";?>

<div class="row clearfix">
		<div class="col-md-3 column">
			<h3><span id="cabecalho_menu_lateral">NEPOPS</span></h3>
			<hr>
		</div>
		<div class="col-md-9 column">
			<h3><span id="titulo_noticia">Biblioteca</span></h3>
		</div>
	</div>
	<div class="row clearfix">
		<div class="col-md-3 column">
			<div class="list-group">
				 
				
					<p class="list-group-item-text">
						<ul class="list-group" id="estilo_menu_lateral">
							<li class="list-group-item"><a href="historia.php?id=11">História</a></li>
							<li class="list-group-item"><a href="projetos.php" style=>Projetos</a></li>
							<li class="list-group-item" ><a href="noticias.php" >Notícias</a></li>
							<li class="list-group-item" style="background-color:#5b62ab;"><a href="biblioteca.php" style="color:white;">Biblioteca</a></li>
							<li class="list-group-item"><a href="photos_nepops.php">Fotos</a></li>
							<li class="list-group-item"><a href="videos.php">Vídeos</a></li>
							<li class="list-group-item"><a href="#">Contatos</a></li>
						</ul>
					</p>
				
				
			</div>
		</div>
		<div class='col-md-9 column'>
		<div id="conteudo">
		<?php
		try{
			include_once "functions.php";

			//$biblioteca = getBibliotecas();
			$biblioteca = $videos = getBibliotecasPaginacao();

			

			foreach ($biblioteca as $key => $value) {

				echo "<div class='row clearfix' style='margin-bottom: 1.2em;'>";
				echo "<div class='col-md-6 column imgProjetos'>";
				//echo "<img alt='140x140' src='data:image/jpeg;base64,".base64_encode($value->logo)."' />";
				echo "<div data-configid='".$value->url."' style='width:370px; height:225px;' class='issuuembed'></div><script type='text/javascript' src='//e.issuu.com/embed.js' async='true'></script>";
				echo "</div>";
				echo "<div class='col-md-6 column'>";
				echo "<h3 class='titleProject' style='margin-top: 0em'><a href='projeto.php?id=".$value->projeto_id."'><span id='tituloProjeto' style='font-family: AvenirNextLTPro-MediumCn;font-size:1.2em;text-indent: 5em;'>".$value->titulo."</span></a></br>";
				echo "<p style='color: #000000'><span id='descricaoProjeto'>".$value->descricao."</span></p>";
				echo "</div>";
				echo "</div>";


			}
			echo "</div>";
		}catch(Exception $e){
			echo "Erro ao listar os projetos.";

		}

		?>
		</div>

	</div>



	<script type="text/javascript">
                <?php echo "var qnt = ".(string)ceil(getCountBibliotecaDados()->count/4).";";?>
                function nextPage(){

                    var page = parseInt($('#page').text());
                    var count = qnt;

                    if(page < count)
                        setPage(page);
                }

                function prevPage(){

                    var page = parseInt($('#page').text());
                    if(page > 1)
                        setPage(page-2);


                }
                function setPage(x){
                    var p =  x + 1;
                    funcao_ajax(p);
                    $( "#page" ).html(p);
                    document.getElementById("conteudo").innerHTML="";
                }

                function funcao_ajax(pagina)
                {
                    var xmlhttp;
                    if (window.XMLHttpRequest)
                    {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp=new XMLHttpRequest();
                    }
                    else
                    {// code for IE6, IE5
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange=function()
                    {
                        if (xmlhttp.readyState==4 && xmlhttp.status==200)
                        {
                            document.getElementById("conteudo").innerHTML=xmlhttp.responseText;

                        }
                    }

                    xmlhttp.open('GET','getAllBiblioteca.php?pagina='+pagina,true);

                    xmlhttp.send();
                }

            </script>


            <div class="row" >

                <div class="col-md-5 column">

                </div>

                <div class="col-md-7 column" >
                    <button style="padding: 0;border: none;background: none;" onclick="prevPage()"><img src="img/seter.png"></button>
                    <span id="page" onchange="funcao_ajax(this)">1</span>
                    <button style="padding: 0;border: none;background: none;" onclick="nextPage()"><img src="img/setdr.png"></button>
                </div>

            </div>

        </div>
    </div>
	


<script>
    $(document).ready(function() {
 
      $("#owl-demo").owlCarousel({
          autoPlay:true,
          navigation : false, // Show next and prev buttons
          slideSpeed : 300,
          paginationSpeed : 400,
          singleItem:true,
          pagination:false,
          scrollPerPage : false,
          paginationNumbers: false,

          // "singleItem:true" is a shortcut for:
          // items : 1, 
          // itemsDesktop : false,
          // itemsDesktopSmall : false,
          // itemsTablet: false,
          // itemsMobile : false
     
      });
 
});
  </script>



<!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">
   
  <!--  jQuery 1.7+  -->
  <script src="owl/assets/js/jquery-1.9.1.min.js"></script>
   
  <!-- Include js plugin -->
  <script src="owl/owl-carousel/owl.carousel.js"></script>


<?php include_once "footer_project.php";?>