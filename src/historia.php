<?php include_once "header_project.php";?>

    <div class="row clearfix">
        <div class="col-md-3 column">
            <h3><span id="cabecalho_menu_lateral"><b><?php echo $projeto[0]->sigla;?></b></span></h3>
            <hr>
        </div>
        <div class="col-md-9 column">
            <h3><span id="titulo_noticia">NEPOPS - História</span></h3>
        </div>
    </div>

    <!--apagar e descomentar o dinamico //echo-->
    <div class='row clearfix'>
    <div class='col-md-3 column'>
        <div class='list-group'>
            <p class="list-group-item-text">

            <ul class="list-group" id="estilo_menu_lateral">
                <li class="list-group-item" style="background-color:#5b62ab;"><a href="historia.php?id=11" style="color:white;">História</a></li>
                <li class="list-group-item"><a href="projetos.php">Projetos</a></li>
                <li class="list-group-item" ><a href="noticias.php" >Notícias</a></li>
                <li class="list-group-item" ><a href="biblioteca.php" >Biblioteca</a></li>
                <li class="list-group-item"><a href="photos_nepops.php">Fotos</a></li>
                <li class="list-group-item"><a href="videos.php">Vídeos</a></li>
                <li class="list-group-item"><a href="#">Contatos</a></li>
            </ul>
            </p>

            <script>
                function funcao_ajax()
                {
                    var xmlhttp;
                    if (window.XMLHttpRequest)
                    {// code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp=new XMLHttpRequest();
                    }
                    else
                    {// code for IE6, IE5
                        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange=function()
                    {
                        if (xmlhttp.readyState==4 && xmlhttp.status==200)
                        {
                            document.getElementById("texto").innerHTML=xmlhttp.responseText;

                        }
                    }
                    <?php
                        //echo "xmlhttp.open('GET','texto.php?id_projeto=".$projeto[0]->id."',true);";
                        echo "xmlhttp.open('GET','texto.php?id_projeto=11',true);";
                    ?>

                    xmlhttp.send();
                }
            </script>

            <?php

            try{

                include_once "functions.php";





                if(true){
                    $id = 11;

                    echo	"</div>";
                    echo	"</div>";
                    echo	"<div class='col-md-9 column'>";


                    $apresentacao = getApresentacaoProjeto($id);
                    echo "<table style='float:left;'>";
                    echo "<tr><td style='padding-right: 6%;'><div id='imgProjetoLogo'><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->logo)."' alt='' class='widthandHeight'></div></td></tr>";
                    $textTotal = strlen($apresentacao[0]->textoApresentacao);
                    if($textTotal > 1500){
                        //print_r($apresentação);

                        //echo	"<tr><td></td><td></td></tr>";
                        echo	"<tr><td><h6><span id='descricao_noticia' ></span></h6></td></tr>";
                        echo "</table>";

                        echo "<div id='texto'>";
                        echo "<span style='font-family: AvenirLTStdMedium;font-size:1em;text-indent: 5em;'>";
                        echo "<p class='textoApresentacaoProjeto' style='padding-left:20px'>".$texto = substr($apresentacao[0]->textoApresentacao,0 , 1500)."</p>";
                        //echo "<a class='read-more-show hide' href='#'>Read More</a> <span class='continueText'><p>".$textoContinue = substr($apresentacao[0]->textoApresentacao, 1500, $textTotal)."</p><a class='read-more-hide hide' href='#'>Read Less</a></span></span>";

                        //echo "<p>Hello</p>";
                        //echo "<p >Good Bye</p>";
                        echo "<button id='btTextContinue' onclick='funcao_ajax()' >continuar lendo</button>";
                        echo "</div>";

                    }else{

                        //print_r($apresentação);
                        //echo "<table style='float:left;'>";
                        //echo	"<tr><td><img src='data:image/jpeg;base64,".base64_encode($apresentacao[0]->logo)."' alt='' style='padding-right: 6%;'></td><td></td></tr>";
                        echo	"<tr><td><h6><span id='descricao_noticia' ></span></h6></td></tr>";
                        echo "</table>";


                        echo "<span style='font-family: AvenirLTStdMedium;font-size:1em;text-indent: 5em;'>";
                        echo "<p>".$apresentacao[0]->textoApresentacao."</p></span>";
                    }
                }
            }catch(Exception $e){
                echo "Não foi possível encontrar o projeto.";
            }

            ?>

            </span>
        </div>
    </div>



    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({
                autoPlay:true,
                navigation : false, // Show next and prev buttons
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem:true,
                pagination:false,
                scrollPerPage : false,
                paginationNumbers: false,

                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    </script>

    <script>
        var flip = 0;
        /*$( "button" ).click(function() {
         $( "#continueText" ).toggle( flip++ % 2 == 0);
         });*/

        // Hide the extra content initially, using JS so that if JS is disabled, no problemo:
        $('.continueText').addClass('hide')
        $('.read-more-show, .read-more-hide').removeClass('hide')

        // Set up the toggle effect:
        $('.read-more-show').on('click', function(e) {
            $(this).next('.continueText').removeClass('hide');
            $(this).addClass('hide');
            e.preventDefault();
        });

        // Changes contributed by @diego-rzg
        $('.read-more-hide').on('click', function(e) {
            var p = $(this).parent('.continueText');
            p.addClass('hide');
            p.prev('.read-more-show').removeClass('hide'); // Hide only the preceding "Read More"
            e.preventDefault();
        });

    </script>

    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.carousel.css">

    <!-- Default Theme -->
    <link rel="stylesheet" href="owl/owl-carousel/owl.theme.css">

    <!--  jQuery 1.7+  -->
    <script src="owl/assets/js/jquery-1.9.1.min.js"></script>

    <!-- Include js plugin -->
    <script src="owl/owl-carousel/owl.carousel.js"></script>

<?php include_once "footer_project.php"; ?>