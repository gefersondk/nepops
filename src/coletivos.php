<?php include_once "header.php"; ?>

    <div class="row clearfix">
        <div class="col-md-3 column">
            <div class="list-group">
                <p class="list-group-item-text">
                <h3><span id="cabecalho_menu_lateral">NEPOPS</span></h3>
                <hr>
                <ul class="list-group" id="estilo_menu_lateral">
                    <li class="list-group-item"><a href="historia.php?id=11">História</a></li>
                    <li class="list-group-item" style="background-color:#5b62ab;"><a href="projetos.php" style="color:white;">Projetos</a></li>
                    <li class="list-group-item"><a href="noticias.php">Notícias</a></li>
                    <li class="list-group-item" ><a href="biblioteca.php" >Biblioteca</a></li>
                    <li class="list-group-item"><a href="photos_nepops.php">Fotos</a></li>
                    <li class="list-group-item"><a href="videos.php">Vídeos</a></li>
                    <li class="list-group-item"><a href="contato_project.php">Contatos</a></li>

                </ul>
                </p>
            </div>
        </div>

        <?php
        try{
            include_once "functions.php";

            $projects = getColetivos();

            echo "<div class='col-md-9 column'>";

            foreach ($projects as $key => $value) {

                echo "<div class='row clearfix' style='margin-bottom: 1.2em;'>";
                echo "<div class='col-md-6 column imgProjetos'>";
                echo "<img alt='140x140' src='data:image/jpeg;base64,".base64_encode($value->logo)."' class='widthandHeight' />";
                echo "</div>";
                echo "<div class='col-md-6 column'>";
                echo "<h3 class='titleProject' style='margin-top: 0em'><a href='projeto.php?id=".$value->id."'><span id='tituloProjeto' style='font-family: AvenirNextLTPro-MediumCn;font-size:1.2em;text-indent: 5em;'>".$value->titulo."</span></a></br>";
                echo "<p style='color: #000000'><span id='descricaoProjeto'>".$value->descricao."</span></p>";
                echo "</div>";
                echo "</div>";


            }
            echo "</div>";
        }catch(Exception $e){
            echo "Erro ao listar os projetos.";

        }

        ?>





    </div>


<?php include_once "footer_project.php"; ?>